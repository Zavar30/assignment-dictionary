global find_word
extern string_equals

section .text

find_word:
    cmp rsi, 0
    je .fail
    add rsi, 8 ; проверка равенства ключей
    call string_equals
    test rax, rax ; 0 - ключи не равны
    jnz .extractAddress ; если нашли, то извлекаем адрес
    sub rsi, 8
    mov rsi, [rsi] ; двигаемся по списку
    jmp find_word

.fail:
    xor rax, rax ; возвращаем ноль в случае отсутствия ключа
    ret

.extractAddress: ; some magic
    sub rsi, 8
    mov rax, rsi
    ret
