global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy
global print_err_string


section .data
nl_char: db 10 ; символ перехода на след. строку
section .text

; Принимает код возврата и завершает текущий процесс
; argumet - rdi
exit:
    mov rax, 60 ; номер exit syscall, код возврата уже в rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; ardument - rdi
string_length:
    xor rax, rax ; обнуляем rax для хранения в неём длины строки
    .loop:
          cmp byte [rdi+rax], 0 ; проверка на нуль-терминатор
          je .end ; если символ - нуль-терминатор, то это конец строки
          inc rax ; увеличиваем счётчик (длину строки)
          jmp .loop ; переход в начало цикла
    .end:
          ret
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; ardument - rdi
print_string:
    call string_length ; узнаём длину строки, в rdi уже лежит строка
    mov rdx, rax ; длина строки
    mov rax, 1 ; номер write syscall
    mov rsi, rdi ; строка вывода
    mov rdi, 1 ; stdout
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
; ardument - rdi
print_err_string:
    call string_length ; узнаём длину строки, в rdi уже лежит строка
    mov rdx, rax ; длина строки
    mov rax, 1 ; номер write syscall
    mov rsi, rdi ; строка вывода
    mov rdi, 2 ; stderr
    syscall
    ret

; Принимает код символа и выводит его в stdout
; argument - rdi
print_char:
    push rdi ; сохраняем символ в стэк
    mov rax, 1 ; номер write syscall
    mov rsi, rsp ; символ для вывода по указателю из rsp
    mov rdi, 1 ; stdout
    mov rdx, 1 ; длина строки (здесь один символ)
    syscall
    pop rdi ; для восстановления rsp
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, nl_char ; вызываем print_char с аргументом nl_char
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp ; сохраняем регистры
    push rbx

    mov rbx, 10 ; делитель, основание системы
    mov rax, rdi ; кладём число в rax
    mov rbp, rsp ; сохраняем rsp
    dec rsp
    .divide:
      xor rdx, rdx ; чистим rdx
      div rbx ; делим на 10
      add dl, '0' ; "делаем" символом
      dec rsp
      mov [rsp], dl ; кладём цифру
      test ax, ax ; проверяем на конец
    jnz .divide

    mov rdi, rsp ; печатаем uint (в rdi указатель начала буфера)
    call print_string
    mov rsp, rbp ; восстановление rsp

    pop rbp ; восстановление регистров
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0 ; определяем знак
    jge .pos ; +
    push rdi ; сохраяем rdi для послед. использования
    mov rdi, '-' ; печатаем -
    call print_char
    pop rdi ; извлекаем оригинальное число
    neg rdi ; инвертируем и печатем как uint
    .pos: ; если +, то просто выводим как uint
      call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r13 ; сохранение регистров
    push r14
    push rcx

    xor rcx, rcx
    xor r13, r13
    xor r14, r14
    .loop:
      mov r13b, byte[rsi + rcx] ; Сохраняем байт первой строки
      mov r14b, byte[rdi + rcx] ; Сохраняем байт второй строки
      cmp r13b, r14b ; Сравниваем байты
      jne .err ; Если не равны, то завершаем работу
      cmp r14b, 0 ; Если нуль-терминант, то завершаем работу
      je .finish
      inc rcx ; увеличиваем индекс
      jmp .loop
    .finish:
      mov rax, 1 ; когда равны возвращаем 1
      jmp .r
    .err:
      mov rax, 0 ; когда не равны возвращаем 0
    .r:
      pop rcx ; восстановление регистров
      pop r14
      pop r13
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0 ; кладем 0, чтобы извлечь его, если будет достигнут конец потока
    mov rax, 0 ; номер read syscall
    mov rdi, 0 ; номер stdin
    mov rsi, rsp ; кладём указатель на место для символа
    mov rdx, 1 ; длина строки (здесь один символ)
    syscall
    pop rax ; в rax кладём результат
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r13 ; сохраняем регистры
    push r12

    xor r12, r12 ; обнуляем r12 и r13
    xor r13, r13
    mov r13, rdi ; указатель на начало буфера
    .first:
      call read_char ; читаем первый символ; если он пробел, таб,
      cmp rax, 0     ; перевод строки, то игнорируем его; при конце потока
      je .done       ; завершаем работу
      cmp rax, 0x9
      je .first
      cmp rax, 0x20
      je .first
      cmp rax, 0xA
      je .first

    .read: ; цикл чтения остальных символов
      cmp rsi, r12 ; проверка на выход за пределы буфера
      je .overflow
      mov [r13 + r12], rax ; кладём символ в буфер
      inc r12 ; увеличиваем смещение (или длину слова)
      call read_char ; читаем очередной символ
      cmp rax, 0 ; если это пробельный символ или конец потока, то
      je .done   ; завершвем работу
      cmp rax, 0x9
      je .done
      cmp rax, 0x20
      je .done
      cmp rax, 0xA
      je .done

      jmp .read

    .done:
      xor rax, rax
      mov [r13 + r12], rax ; записываем нуль-терминатор
      mov rax, r13 ; записываем адрес буфера
      mov rdx, r12 ; записываем длину слова
      jmp .finish
    .overflow:
      xor rax, rax ; обнуляем rax
    .finish:
      pop r12 ; восстановление регистров
      pop r13
      ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r13

    xor r13, r13
    xor rdx, rdx ; очищаем для длины
    xor rax, rax ; очищаем для числа

    .uint:
      cmp byte[rdi + rdx], '0' ; если код меньше 0, то это не цифра
      jl .r
      cmp byte[rdi + rdx], '9' ; если код больше 9, то это не цифра
      jg .r

      mov r13b, byte[rdi + rdx] ; сохраняем символ
      sub r13b, '0' ; вычитаем код нуля, чтобы получить цифру
      imul rax, 10 ; умножаем на 10, чтобы "появилось место для цифры"
      add rax, r13 ; добавляем цифру
      inc rdx ; увеличиваем длину

      jmp .uint

    .r:
      pop r13
      ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось

parse_int:
    cmp byte[rdi], '-'
    jne .positive
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
    .positive:
      call parse_uint
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; argumets - rdi, rsi, rdx
string_copy:
  push rbx ; сохраняем rbx
  xor rax, rax ; обнуляем rax
  .loop:
      cmp rax, rdx ; сравниваем длину строки (rax) с размером буфера
      je .fail     ; если вышли за пределы буфера - возвращаем 0
      mov rbx, [rdi + rax] ; перемещаем очередной симол в rbx
      mov [rsi + rax], rbx ; перемещаем очередной симол в буфер
      cmp rbx, 0 ; если rbx = 0, то достигнут конец строки
      je .exit
      inc rax ; увеличить rax (длину строки)
      jmp .loop
  .fail:
      xor rax, rax
      jmp .exit
  .exit:
      pop rbx ; восстановление rbx
      ret
